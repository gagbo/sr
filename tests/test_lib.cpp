#include "gtest/gtest.h"
#include "sr/cdn_middleware.hpp"
#include "sr/peer_client.hpp"

TEST(BasicTest, DefaultConstructor) {
  sr::CdnMiddleware instance("Test");
  EXPECT_EQ(instance.get_url(), "Test");
}

TEST(Message, ParsingConstructor) {
  sr::Message instance("@Alice:Test");
  EXPECT_EQ(instance.body, "Test");
  EXPECT_EQ(instance.recipient, "Alice");

  sr::Message instance_with_spaces("@Alice:  Test");
  EXPECT_EQ(instance_with_spaces.body, "Test");
  EXPECT_EQ(instance_with_spaces.recipient, "Alice");

  sr::Message instance_with_space("@Bob: Alice is not a pest");
  EXPECT_EQ(instance_with_space.body, "Alice is not a pest");
  EXPECT_EQ(instance_with_space.recipient, "Bob");
}

// TODO: input/payload validation on route methods
