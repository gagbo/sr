# Building

This project needs `libcurl` headers to compile (because of the restclient
dependency).

`libasan` is also a dependency if you build the `Debug` artifacts.

Otherwise it is a cmake project, so after running `cmake` and `make`, you will
have executables in the `app` subdirectory of the build folder.

## Tests

Very few tests have been added to the `tests/` folder and can be run using
`ctest`.


# CDN middleware

``` sh
./apps/cdn URL
```

This will pop a server that listens to port 8080, so you can use
`localhost:8080` for a media player.

# P2P messaging

``` sh
# In one terminal
./apps/server

# In another terminal
./apps/client 8081 Alice http://localhost:8080

# In yet another terminal
./apps/client 8082 Bob http://localhost:8080
```

Typing

> @Alice: Hello Alice!!

in Bob's terminal will send the message to Alice, which will show directly on
stdout.


In order to send a message, you need to use the syntax `"@Recipient: message"`,
it will find `Recipient` through registry, and then directly use this URL to
send `message` to them.

The protocol is quite simple:
- each client must register to the registry, with a unique identifier and the
  URL it listens to for messages.
- each client can therefore query the registry for an identifier, and then use
  the URL to communicate.

## Limitations

The protocol only works on HTTP (not "s"), with `text/plain` payloads/messages.

## Scaling and bottlenecks

Currently each sent message requires a request to the registry for the peer URL.
Having a more stateful client can lighten the load on the server.

Each client is running the server that is supposed to listen for incoming
messages. This HTTP server doesn't run behind a load-balancer (to keep
client-side "small"), so technically a client could be DDoS'd if it receives way
too many messages.

The state of the registry, in this implementation, is stored in memory (the
`peers` unordered_map). This means it is impossible to scale horizontally the
registry if we need to scale to a really high number of requests, and
externalizing this state in a NoSQL database component is going to be more
efficient if we really need this at some point.

## Attack surface

Currently there is literally 0 authorization/authentication implemented:
- Anyone can register as anyone else on the registry
- Anyone can request the URL of any other peer on the registry

No encryption is done either:
- Peer to Peer communication use clear text HTTP
- Requests to registry are clear text as well

Using a certificate system (PKI-like) to give trust levels to users
communicating with the registry could be helpful, and adding in registry-state a
mean to store user certificates as well is useful to help provide end-to-end
encryption for peers.

## Compatibility

The protocol relies on HTTP only, so it should be possible to implement it
everywhere, even if there is going to be some overhead.

This implementation in particular needs pthread and libcurl in order to work, at
least.

