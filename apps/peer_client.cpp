#include "sr/peer_client.hpp"
#include "sr/Version.hpp"

#include <iostream>
#include <string>
#include <thread>

int main(int argc, char *argv[]) {
  int port = 8080;
  if (argc <= 3) {
    return 1;
  }
  port = std::stoi(std::string(argv[1]));
  auto user_id = std::string(argv[2]);
  auto registry_url = std::string(argv[3]);

  sr::PeerClient instance(user_id, port, registry_url);

  std::thread rx_thread(&sr::PeerClient::serve, &instance);

  std::string message;
  while (message != "q") {
    std::cout << "> ";
    std::getline(std::cin, message);
    sr::Message msg(message);
    instance.send(msg);
    message.clear();
  }

  rx_thread.join();

  return 0;
}
