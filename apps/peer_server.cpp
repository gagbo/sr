#include "sr/peer_server.hpp"
#include "sr/Version.hpp"

#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
  int port = 8080;
  if (argc > 1) {
      port = std::stoi(std::string(argv[1]));
  }

  sr::PeerServer instance(port);
  instance.serve();
  return 0;
}
