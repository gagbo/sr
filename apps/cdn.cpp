#include "sr/cdn_middleware.hpp"
#include "sr/Version.hpp"

#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
  std::string url = "https://storage.googleapis.com";
  if (argc > 1) {
      url = std::string(argv[1]);
  }

  sr::CdnMiddleware instance(url);
  instance.display();
  instance.serve(8080);
  return 0;
}
