add_library(srlib STATIC cdn_middleware.cpp ../include/sr/cdn_middleware.hpp)
target_include_directories(srlib PUBLIC ../include)
target_compile_features(srlib PUBLIC cxx_std_17)

add_library(peer STATIC peer_server.cpp ../include/sr/peer_server.hpp
  peer_client.cpp ../include/sr/peer_client.hpp)
target_include_directories(peer PUBLIC ../include)
target_compile_features(peer PUBLIC cxx_std_17)

fetchcontent_getproperties(restclient-cpp)
target_include_directories(srlib PUBLIC ${restclient-cpp_BINARY_DIR}/include)
target_link_directories(srlib PUBLIC ${restclient-cpp_BINARY_DIR}/lib)
target_link_libraries(srlib PUBLIC restclient-cpp)

target_include_directories(peer PUBLIC ${restclient-cpp_BINARY_DIR}/include)
target_link_directories(peer PUBLIC ${restclient-cpp_BINARY_DIR}/lib)
target_link_libraries(peer PUBLIC restclient-cpp)

add_dependencies(srlib restclient-cpp-dep)
add_dependencies(peer restclient-cpp-dep)
