#include "sr/cdn_middleware.hpp"

#include "restclient-cpp/restclient.h"
#include <chrono>
#include <iostream>

namespace {
static std::string MANIFEST = "[MANIFEST]";
static std::string SEGMENT = "[SEGMENT]";
static std::string TRACK_SWITCH = "[TRACK SWITCH]";
} // namespace

namespace sr {

CdnMiddleware::CdnMiddleware(std::string &url) : stream_url(url), cli(nullptr) {
  init();
}
CdnMiddleware::CdnMiddleware(const char *url) : stream_url(url), cli(nullptr) {
  init();
}

void CdnMiddleware::display() const { std::cout << stream_url << "\n"; }

void CdnMiddleware::init() {

  RestClient::init();

  // To log
  // std::cout << "server path " << stream_path << std::endl;
  // std::cout << "server server " << stream_server << std::endl;

  cli = std::make_unique<RestClient::Connection>(stream_url.data());
  cli->FollowRedirects(true);

  svr.set_error_handler([](const auto &, auto &res) {
    auto fmt = "Error Status: %d";
    char buf[BUFSIZ];
    snprintf(buf, sizeof(buf), fmt, res.status);
    res.set_content(buf, "text/plain");
  });

  svr.set_exception_handler([](const auto &, auto &res, std::exception &e) {
    res.status = 500;
    auto fmt = "Error 500: %s";
    char buf[BUFSIZ];
    snprintf(buf, sizeof(buf), fmt, e.what());
    res.set_content(buf, "text/plain");
  });
  svr.Get(R"(/.*)", [this](const auto &req, auto &res) {
    return this->interceptor(req, res);
  });
}

void CdnMiddleware::serve(int port) { svr.listen("0.0.0.0", port); }

static std::string infer_request_type(const httplib::Request &req) {
  if (req.path.substr(req.path.size() - 4) == "m3u8") {
    return MANIFEST;
  }
  return SEGMENT;
}

void CdnMiddleware::interceptor(const httplib::Request &req,
                                httplib::Response &res) {

  // Pre: Get type and start time
  std::string request_type = infer_request_type(req);
  if (was_in_segment && request_type == MANIFEST) {
    std::cout << TRACK_SWITCH << std::endl;
    was_in_segment = false;
  }
  if (request_type == SEGMENT) {
    was_in_segment = true;
  }

  auto path = req.path.data();
  std::cout << "[IN]" << request_type << " " << stream_url << path << std::endl;
  const std::chrono::time_point<std::chrono::steady_clock> start =
      std::chrono::steady_clock::now();

  RestClient::HeaderFields headers;
  headers["Accept"] = "*/*";
  cli->SetHeaders(headers);

  // To Log (body, code, headers or request and response)
  auto r = cli->get(path);
  for (const auto &p : r.headers) {
    res.set_header(p.first.data(), p.second);
  }

  res.set_content(r.body, r.headers.at("content-type").data());

  // Post: Log status code and duration
  const auto end = std::chrono::steady_clock::now();
  std::cout << "[OUT]" << request_type << " " << stream_url << path << " ("
            << (end - start) / std::chrono::milliseconds(1) << "ms; " << r.code
            << " — " << r.headers.at("content-length") << ")" << std::endl;
}
} // namespace sr
