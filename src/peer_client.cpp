#include "sr/peer_client.hpp"
#include "restclient-cpp/restclient.h"
#include <iostream>
#include <sstream>

namespace sr {
Message::Message(const std::string &format_str) {
  if (format_str[0] != '@') {
    return;
  }

  auto found = format_str.find(':');
  recipient = format_str.substr(1, found - 1);
  found = format_str.find_first_not_of(" ", found + 1);
  body = format_str.substr(found);
}

std::string Message::to_string() const {
  std::ostringstream oss;
  oss << sender << "@" << body;
  return oss.str();
}

std::ostream &operator<<(std::ostream &os, const Message &msg) {
  os << "[FROM " << msg.sender << "]: " << msg.body << std::endl;
  return os;
}

PeerClient::PeerClient(std::string _name, int _port, std::string _registry)
    : username(_name), port(_port), registry(_registry) {
  register_self();
  svr.Post("/message", [this](const auto &req, auto &res) {
    return this->receive_message(req, res);
  });
}

std::string PeerClient::fetch_peer_url(std::string peer_id) const {
  auto response = RestClient::get(registry + "/users/" + peer_id);
  if (response.code >= 400) {
    return "";
  }
  return response.body;
}

bool PeerClient::register_self() const {
  auto response = RestClient::post(
      registry + "/register", "text/plain",
      username + "@" + "http://localhost:" + std::to_string(port));
  if (response.code == 201) {
    return true;
  }
  std::cerr << "Registration failed, got " << std::to_string(response.code)
            << std::endl;
  return false;
}

void PeerClient::send(Message &msg) const {
  msg.sender = username;
  auto rec_url = fetch_peer_url(msg.recipient);
  if (rec_url.empty()) {
    std::cerr << "No peer found for " << msg.recipient << std::endl;
    return;
  }
  auto response = RestClient::post(rec_url + "/message", "text/plain",
                                   msg.sender + "@" + msg.body);
  if (response.code != 200) {
    std::cerr << "Sending message failed: " << response.code << std::endl;
  }
}

void PeerClient::serve() { svr.listen("0.0.0.0", port); }

static Message parse_body_to_msg(const std::string &body) {
  auto found = body.find("@");
  auto user = body.substr(0, found);
  auto payload = body.substr(found + 1);
  Message retval;
  retval.sender = user;
  retval.body = payload;
  return retval;
}

void PeerClient::receive_message(const httplib::Request &req,
                                 httplib::Response &res) {
  std::cerr << "Received a message !" << std::endl;
  auto msg = parse_body_to_msg(req.body);
  msg.recipient = username;
  std::cout << msg << std::endl;
  res.status = 200;
}
} // namespace sr
