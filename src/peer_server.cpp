#include "sr/peer_server.hpp"

namespace sr {
PeerServer::PeerServer(int _port) : port(_port) {
  svr.Get(R"(/users/(.*))", [this](const auto &req, auto &res) {
    return this->fetch_user_url(req, res);
  });

  svr.Post("/register", [this](const auto &req, auto &res) {
    return this->register_user(req, res);
  });
}

void PeerServer::serve() { svr.listen("0.0.0.0", port); }

static std::pair<std::string, std::string> parse_body_to_user_url(const std::string& body) {
        auto found = body.find("@");
        auto user = body.substr(0, found);
        auto url = body.substr(found+1);
  return std::make_pair<std::string, std::string>(std::move(user), std::move(url));

}

void PeerServer::register_user(const httplib::Request &req,
                               httplib::Response &res) {

  auto pair = parse_body_to_user_url(req.body);
  if (peers.find(pair.first) != peers.end()) {
    res.status = 400;
    res.set_content("user_id already exists", "text/plain");
    return;
  }

  peers.insert(pair);
  std::cerr << pair.first << " registered" << std::endl;
  res.status = 201;
}

void PeerServer::fetch_user_url(const httplib::Request &req,
                                httplib::Response &res) {
  auto user_id = req.matches[1];
  auto found = peers.find(user_id);
  if (found == peers.end()) {
    std::cerr << user_id << " missing" << std::endl;
    res.status = 404;
    return;
  }

  res.set_content(found->second, "text/plain");
}

} // namespace sr
