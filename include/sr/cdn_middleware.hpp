#pragma once

#include "extern/httplib.h"
#include "restclient-cpp/connection.h"
#include <memory>
#include <string>

namespace sr {
/// A middleware between a player like VLC and a Content Distribution Network
/// (CDN)
class CdnMiddleware {
public:
  CdnMiddleware(std::string &url);
  CdnMiddleware(const char *url);
  /// Display a human readable representation of the Middleware
  void display() const;
  /// Get the URL of the underlying stream
  inline std::string get_url() const { return stream_url; }
  /// Serve the middleware on localhost listening to PORT
  void serve(int port);

  ~CdnMiddleware() { RestClient::disable(); }

private:
  void interceptor(const httplib::Request &req, httplib::Response &res);
  void init();
  httplib::Server svr;
  std::string stream_url;
  std::unique_ptr<RestClient::Connection> cli;
  bool was_in_segment{false};
};
} // namespace sr
