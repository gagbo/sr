#pragma once

#include <unordered_map>
#include <string>

#include "extern/httplib.h"

namespace sr {
class PeerServer {
public:
  PeerServer(int port);
  void serve();

private:
  // Controller for the peer network
  httplib::Server svr;
  int port;

  // Maps unique identifiers to a Client URL
  std::unordered_map<std::string, std::string> peers;

  // Routes
  /// POST /register user_id@user_url
  void register_user(const httplib::Request &req, httplib::Response &res);
  /// GET /users/:id
  void fetch_user_url(const httplib::Request &req, httplib::Response &res);
};
} // namespace sr
