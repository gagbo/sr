#pragma once

#include <string>
#include <unordered_map>
#include <iostream>

#include "extern/httplib.h"

namespace sr {
struct Message {
  Message() = default;
  /// Parse a string in format
  /// @user:message
  /// in a Message. On failure the Message will have no recipient
  Message(const std::string& format_str);
  std::string to_string() const;

  std::string sender;
  std::string recipient;
  std::string body;

  friend std::ostream& operator<<(std::ostream&, const Message&);
};


class PeerClient {
public:
  PeerClient(std::string username, int port, std::string registry);
  void serve();
  void send(Message &msg) const;
  bool register_self() const;
  std::string fetch_peer_url(std::string peer_id) const;

private:
  // Listens to incoming messages
  httplib::Server svr;
  std::string username;
  int port;
  std::string registry;

  // Routes
  /// POST /message from@message
  void receive_message(const httplib::Request &req, httplib::Response &res);
};
} // namespace sr
